// simple example for pthreads
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include<unistd.h>

#define NUM_THREADS     4
#define NUM_SIM         1

int kollisionen;
long thread_start[NUM_THREADS];
long thread_end[NUM_THREADS];

int level[NUM_THREADS];
int last[NUM_THREADS];
// int favoured;

int lock (long tid)
{
  int i, j, loop;

  for (i = 1; i < NUM_THREADS; i++) {
    level[tid] = i;
    last[i] = tid;
    loop = 1;
    while ((loop)&&(last[i] == tid)) {
      j = 0;
      loop = 0; 
      while ((j < NUM_THREADS)&&((level[j] < i)||(j == tid)))
        j++;
      if (j < NUM_THREADS)
        loop = 1;
    }
  } 
  return 0;
}

int unlock (long tid)
{
  level[tid] = 0;
  return 0;
}


void *driveAuto(void *threadid)
{
  long tid;
  tid = (long)threadid;
  struct timeval tv;
  
  lock (tid); //Beginn kritischer Abschnitt
  
  gettimeofday(&tv,NULL); //Zeitpunkt, wann das Auto losfaehrt.
  thread_start[tid]=tv.tv_usec; //Speichert Startzeitpunkt von Auto pid

  // printf("Hehe Car %ld is driving\n", tid);
  sleep(0.001); //Auto faehrt ueber Bruecke

  gettimeofday(&tv,NULL); //Zeitpunkt, wann das Auto ankommt.
  thread_end[tid]=tv.tv_usec; //Speichert Endzeitpunkt von Auto pid

  unlock (tid); //Ende kritischer Abschnitt
  
  pthread_exit (NULL);
}

int main (void)
{
  pthread_t threads[NUM_THREADS];
  int rc;
  long t;
  
  //inital data
  for (int i = 0; i < NUM_THREADS; i++) {
    level[i] = 0;
    last[i] = 0;
  }
  kollisionen = 0;

  for(int i= 0;i<NUM_SIM;i++){ //Anzahl der Simulationen
    for(t=0; t < NUM_THREADS; t++) {
      // printf ("In main: creating thread %ld\n", t);
      rc = pthread_create (&threads[t], NULL, driveAuto, (void *)t);
      if (rc) {
        printf ("ERROR; return code from pthread_create () is %d\n", rc);
        exit (-1);
      }
    }
  
    // joining threads
    for (t = 0; t < NUM_THREADS; t++) {
      pthread_join (threads[t], NULL);
    }

    //berechne, ob Autos kollidiert sind
    
    for(int k=0;k<NUM_THREADS;k++){
      for (int l=0;l<NUM_THREADS;l++) {
        if ((thread_start[k] < thread_end[l] && thread_start[k] > thread_start[l]) ) {
          kollisionen++;
        }
      }
    }
    


  }


  printf("Simulationen: %d\n",NUM_SIM);
  printf("Kollisionen: %d\n",kollisionen);
    // for(int i=0;i<NUM_THREADS;i++){
    //   printf("%ld , %ld\n", thread_start[i], thread_end[i]);
    // }
    //
  /* Last thing that main() should do */
  pthread_exit(NULL);
}
