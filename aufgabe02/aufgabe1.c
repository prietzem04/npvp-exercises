// simple example for pthreads
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include<unistd.h>

#define NUM_THREADS     2
#define NUM_SIM         100000

int kollisionen;
long thread_start[2];
long thread_end[2];

void *driveAuto(void *threadid)
{
  long tid;
  tid = (long)threadid;
  struct timeval tv;
  
  gettimeofday(&tv,NULL); //Zeitpunkt, wann das Auto losfaehrt.
  thread_start[tid]=tv.tv_usec; //Speichert Startzeitpunkt von Auto pid

  // printf("Hehe Car %ld is driving\n", tid);
  sleep(0.001); //Auto faehrt ueber Bruecke

  gettimeofday(&tv,NULL); //Zeitpunkt, wann das Auto ankommt.
  thread_end[tid]=tv.tv_usec; //Speichert Endzeitpunkt von Auto pid

  pthread_exit (NULL);
}

int main (void)
{
  pthread_t threads[NUM_THREADS];
  int rc;
  long t;
  kollisionen = 0;

  for(int i= 0;i<NUM_SIM;i++){ //Anzahl der Simulationen
    for(t=0; t < NUM_THREADS; t++) {
      // printf ("In main: creating thread %ld\n", t);
      rc = pthread_create (&threads[t], NULL, driveAuto, (void *)t);
      if (rc) {
        printf ("ERROR; return code from pthread_create () is %d\n", rc);
        exit (-1);
      }
    }
  
    // joining threads
    for (t = 0; t < NUM_THREADS; t++) {
      pthread_join (threads[t], NULL);
    }

    //berechne, ob Autos kollidiert sind
    if ((thread_start[0] < thread_end[1] && thread_start[0] > thread_start[1]) || (thread_start[1] < thread_end[0] && thread_start[1] > thread_start[0]) ) {
      kollisionen++;
    }

  }


  printf("Simulationen: %d\n",NUM_SIM);
  printf("Kollisionen: %d\n",kollisionen);
    // for(int i=0;i<NUM_THREADS;i++){
    //   printf("%ld , %ld\n", thread_start[i], thread_end[i]);
    // }
    //
  /* Last thing that main() should do */
  pthread_exit(NULL);
}
