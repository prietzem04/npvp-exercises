// simple example for pthreads
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include<unistd.h>

#define NUM_THREADS     4
#define NUM_SIM         10000

int kollisionen;
long thread_start[NUM_THREADS];
long thread_end[NUM_THREADS];
pthread_mutex_t lock;

int level[NUM_THREADS];
int last[NUM_THREADS];
// int favoured;

void *driveAuto(void *threadid)
{
  long tid;
  tid = (long)threadid;
  int _error = 0;
  struct timeval tv;
  
  //Beginn kritischer Abschnitt
  _error = pthread_mutex_lock(&lock);
  
  gettimeofday(&tv,NULL); //Zeitpunkt, wann das Auto losfaehrt.
  thread_start[tid]=tv.tv_usec; //Speichert Startzeitpunkt von Auto pid

  // printf("Hehe Car %ld is driving\n", tid);
  sleep(0.001); //Auto faehrt ueber Bruecke

  gettimeofday(&tv,NULL); //Zeitpunkt, wann das Auto ankommt.
  thread_end[tid]=tv.tv_usec; //Speichert Endzeitpunkt von Auto pid

  _error = pthread_mutex_unlock(&lock);
  //Ende kritischer Abschnitt
  
  pthread_exit (NULL);
}

int main (void)
{
  pthread_t threads[NUM_THREADS];
  int rc;
  long t;
  
  //inital data
  for (int i = 0; i < NUM_THREADS; i++) {
    level[i] = 0;
    last[i] = 0;
  }
  kollisionen = 0;
  
  // init lock
  pthread_mutex_init(&lock, NULL);

  for(int i= 0;i<NUM_SIM;i++){ //Anzahl der Simulationen
    for(t=0; t < NUM_THREADS; t++) {
      // printf ("In main: creating thread %ld\n", t);
      rc = pthread_create (&threads[t], NULL, driveAuto, (void *)t);
      if (rc) {
        printf ("ERROR; return code from pthread_create () is %d\n", rc);
        exit (-1);
      }
    }
  
    // joining threads
    for (t = 0; t < NUM_THREADS; t++) {
      pthread_join (threads[t], NULL);
    }

    //berechne, ob Autos kollidiert sind
    
    for(int k=0;k<NUM_THREADS;k++){
      for (int l=0;l<NUM_THREADS;l++) {
        if ((thread_start[k] < thread_end[l] && thread_start[k] > thread_start[l]) ) {
          kollisionen++;
        }
      }
    }
    


  }


  printf("Simulationen: %d\n",NUM_SIM);
  printf("Kollisionen: %d\n",kollisionen);
    // for(int i=0;i<NUM_THREADS;i++){
    //   printf("%ld , %ld\n", thread_start[i], thread_end[i]);
    // }
    //
  /* Last thing that main() should do */
  pthread_exit(NULL);
}
